package cn.edu.dgut.css.sai.security.oauth2.client.userinfo;

import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserService;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;

/**
 * {@link OAuth2UserService}委托类
 *
 * @author sai
 * @since 2.2
 */
public final class DelegateSaiOAuth2UserService implements OAuth2UserService<OAuth2UserRequest, OAuth2User> {

    @Override
    public OAuth2User loadUser(OAuth2UserRequest userRequest) throws OAuth2AuthenticationException {
        return switch (userRequest.getClientRegistration().getRegistrationId()) {
            case "qq", "dgut", "weixin", "wxmp" -> new SaiCommonOAuth2UserService().loadUser(userRequest);
            case "dingding" -> new SaiDingDingOAuth2UserService().loadUser(userRequest);
            case "gitee" -> new SaiGiteeOAuth2UserService().loadUser(userRequest);
            default -> new DefaultOAuth2UserService().loadUser(userRequest);
        };
    }
}
